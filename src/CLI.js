const { data } = require('../data/data')
const { filterData, enrichCountData } = require('./DataManager')
const start = () => {
    const parsedFiltersArgs = process.argv.filter(arg => arg.split('=')[0] === '--filter').map(filter => filter.split('=')[1])
    const parsedCountArg = process.argv.filter(arg => arg === '--count')
    let filteredData = filterData(data, parsedFiltersArgs)
    if (parsedCountArg){
        filteredData = enrichCountData(filteredData)
    }
    printResult(filteredData)
}


const printResult = (data) => {
    console.log(JSON.stringify(data))
}

module.exports = {
    start
}