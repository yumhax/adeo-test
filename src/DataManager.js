const enrichCountData = (data) => {
    if (!data) return

    return data.map(country => {
        return {
            name:`${country.name} [${country.people.length}]`,
            people: country.people.map(person => {
                return {
                    ...person,
                    name:`${person.name} [${person.animals.length}]`
                }
            })
        }
    })
}
const filterData = (data, filters) => {
    let filteredData = data
    for (const filter of filters){
       filteredData = filteredData.map(country => {
           const filteredPeople = country.people.map(people => {
               return {...people, animals: people.animals.filter((animal) => {
                   return animal.name.toLowerCase().includes(filter.toLowerCase())
               })}
           }).filter(people => people.animals.length > 0)
           return {...country, people: filteredPeople}
       }).filter(country => country.people.length > 0)
    }

    if (filteredData.length > 0){
        return filteredData
    }
    
    
}

module.exports = {
    filterData,
    enrichCountData
}