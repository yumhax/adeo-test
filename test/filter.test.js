const { filterData, enrichCountData } = require("../src/DataManager");
const { data } = require('../data/data')

const EXPECTED_FILTER_RY = [
    {
      name: 'Uzuzozne',
      people: [
        {
          name: 'Lillie Abbott',
          animals: [
            {
              name: 'John Dory'
            }
          ]
        }
      ]
    },
    {
      name: 'Satanwi',
      people: [
        {
          name: 'Anthony Bruno',
          animals: [
            {
              name: 'Oryx'
            }
          ]
        }
      ]
    }
  ]

const EXPECTED_EMPTY = undefined
const EXPECTED_FILTER_BADGER__COUNT = [
    {
      "name": "Dillauti [2]",
      "people": [
        {
          "name": "Winifred Graham [1]",
          "animals": [
            {
              "name": "Badger"
            }
          ]
        },
        {
          "name": "Bobby Ristori [1]",
          "animals": [
            {
              "name": "Badger"
            }
          ]
        }
      ]
    }
  ]

test('filtering data with filter=ry returns expected data', () => {
    expect(filterData(data, ['ry'])).toStrictEqual(EXPECTED_FILTER_RY);
});
test('filtering data with filter=azeaze returns no result', () => {
    expect(filterData(data, ['azeaze'])).toStrictEqual(EXPECTED_EMPTY);
});
test('filtering data empty returns full data', () => {
    expect(filterData(data, [])).toStrictEqual(data)
})
test('filtering data with filter=Badger and --count returns expected data', () => {
    expect(enrichCountData(filterData(data, ['Badger']))).toStrictEqual(EXPECTED_FILTER_BADGER__COUNT)
})
test('filtering data with filter=azeaze and --count returns no result', () => {
    expect(enrichCountData(filterData(data, ['azeaze']))).toStrictEqual(EXPECTED_EMPTY)
})
test('filtering data with filter=badger returns entries with name: "Badger"', () => {
    expect((filterData(data, ['Badger']))).toStrictEqual(filterData(data, ['badger']))
})
test('filtering data with filter=badger and --count returns entries with name: "Badger"', () => {
  expect((enrichCountData(filterData(data, ['Badger'])))).toStrictEqual(enrichCountData(filterData(data, ['Badger'])))
})
test('filtering data with filter=BADGER and --count returns entries with name: "badger"', () => {
  expect((enrichCountData(filterData(data, ['BADGER'])))).toStrictEqual(enrichCountData(filterData(data, ['badger'])))
})