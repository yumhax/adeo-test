# adeo-test

This program provides the user with a CLI that enables them to filter the given dataset and add contextual information (animal/people count) to it.

## Usage

### Run
```node app.js --filter=<your filter> [--count]```

### Tests
```npm run test```
